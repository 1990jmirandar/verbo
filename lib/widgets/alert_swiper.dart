import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:verbo/model/alertas.dart';

class AlertaSwiper extends StatelessWidget {
  final List<Alertas> listAlert;

  AlertaSwiper({@required this.listAlert});

  var alertStyle = AlertStyle(
    animationType: AnimationType.fromTop,
    isCloseButton: false,
    isOverlayTapDismiss: false,
    animationDuration: Duration(milliseconds: 400),
    alertBorder: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(7.0),
      side: BorderSide(
        color: Colors.grey,
      ),
    ),
    titleStyle: TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.bold
    ),
  );

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      //margin: EdgeInsets.fromLTRB(0, size.height * 0.05, 0, 0),
      child: new Swiper(
        loop: false,
        index: 0,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      child: InkWell(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: FadeInImage.assetNetwork(
                        image: listAlert[index].imagen,
                        placeholder: 'assets/images/source.gif',
                        fit: BoxFit.fill,
                        height: size.height * 0.3,
                      ),
                    ),
                    onTap: () {
                      Alert(
                        context: context,
                        style: alertStyle,
                        title: listAlert[index].text,
                        desc: listAlert[index].contenido,
                        image: Image.network(listAlert[index].imagen),
                        buttons: [
                          DialogButton(
                            child: Text(
                              "Entendido",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                            onPressed: () => Navigator.pop(context),
                            color: Color.fromRGBO(14, 20, 32, 1),
                            radius: BorderRadius.circular(0.0),
                          ),
                        ],
                      ).show();
                    },
                  )),
                  Container(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      listAlert[index].text,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ]),
          );
        },
        itemCount: listAlert.length,
        viewportFraction: 0.7,
        scale: 0.9,
      ),
    );
  }
}

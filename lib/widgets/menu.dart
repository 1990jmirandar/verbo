import 'package:flutter/material.dart';
import 'package:verbo/model/tipo.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:verbo/pages/content_page.dart';
import 'package:verbo/pages/lista_anuncios_page.dart';
import 'package:verbo/pages/lista_devocionales_page.dart';
import 'package:verbo/pages/lista_informativos_page.dart';
import 'package:verbo/pages/lista_musica_page.dart';
import 'package:verbo/pages/lista_predica.dart';
import 'package:verbo/utils/class_builder.dart';
import 'package:verbo/utils/device.dart';

class Menu extends StatelessWidget {
  final List<Tipo> listTipo;
  Menu({@required this.listTipo});
  @override
  Widget build(BuildContext context) {
    return new KFDrawer(
      controller:
          new KFDrawerController(initialPage: ClassBuilder.fromString(), items: getMenu()),
      header: Align(
          alignment: Alignment.centerLeft,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            width: Device.isPhone() ? MediaQuery.of(context).size.width * 0.6 : MediaQuery.of(context).size.width * 0.2,
            child: Image.asset(
              'assets/images/logo.png',
              alignment: Alignment.centerLeft,
            ),
          ),
        ),
      footer: KFDrawerItem(
        text: Text(
          '',
        ),
      ),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Colors.white, Color.fromRGBO(14, 20, 32, 1)],
          tileMode: TileMode.repeated,
        ),
      ),
    );
  }

  List<KFDrawerItem> getMenu() {
    List<KFDrawerItem> lista = new List();
    KFDrawerItem item = new KFDrawerItem.initWithPage(
        text:
            Text("Inicio", style: TextStyle(color: Colors.white, fontSize: 20)),
        page: ContentPage(),
        icon: Icon(Icons.home,color: Colors.white,),

    );
    lista.add(item);

    KFDrawerItem item4 = new KFDrawerItem.initWithPage(
      text: Text("Casa de Amistad",
          style: TextStyle(color: Colors.white, fontSize: 20)),
      page: InformativosPage(),
      icon: Icon(Icons.label,color: Colors.white,),

    );
    lista.add(item4);

    KFDrawerItem item5 = new KFDrawerItem.initWithPage(
      text: Text("Ministerios",
          style: TextStyle(color: Colors.white, fontSize: 20)),
      page: MusicaPage(),
      icon: Icon(Icons.label,color: Colors.white,),

    );
    lista.add(item5);

    KFDrawerItem item2 = new KFDrawerItem.initWithPage(
      text:
          Text("Predicas", style: TextStyle(color: Colors.white, fontSize: 20)),
      page: ListaPredicaPage(),
      icon: Icon(Icons.label,color: Colors.white,),

    );
    lista.add(item2);
    

    KFDrawerItem item3 = new KFDrawerItem.initWithPage(
      text: Text("Devocionales",
          style: TextStyle(color: Colors.white, fontSize: 20)),
      page: DevocionalesPage(),
      icon: Icon(Icons.label,color: Colors.white,),

    );
    lista.add(item3);

    KFDrawerItem item6 = new KFDrawerItem.initWithPage(
      text: Text("Notificaciones",
          style: TextStyle(color: Colors.white, fontSize: 20)),
      page: AnunciosPage(),
      icon: Icon(Icons.label,color: Colors.white,),

    );
    lista.add(item6);

    return lista;
  }
}

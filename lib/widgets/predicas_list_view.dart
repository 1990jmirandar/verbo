import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:verbo/model/predicas.dart';
import 'package:verbo/pages/predicas_page.dart';
import 'package:verbo/utils/class_builder.dart';
import 'package:verbo/utils/device.dart';

class PredicasListView extends StatelessWidget {
  final List<Predicas> listPredicas;
  final int idTipo;
  final String currentPage;

  PredicasListView(
      {@required this.listPredicas, this.idTipo, this.currentPage}) {
    ClassBuilder.page = this.currentPage;
  }

  @override
  Widget build(BuildContext context) {

    return new ListView(
      children: <Widget>[
        for (var item in listPredicas)
          if (idTipo == item.ciTipoPredica) getCard(context, item)
      ],
    );
  }

  Widget getCard(BuildContext context, Predicas predica) {
    final _screenSize = MediaQuery.of(context).size;
    final planetThumbnail = new Container(
        margin: EdgeInsets.fromLTRB(Device.isPhone() ? 0 : 20, _screenSize.height * 0.05, 0, 0),
        child: CircleAvatar(
          backgroundColor: Color.fromRGBO(8, 28, 37, 1),
          backgroundImage: predica.txRutaImagen.isEmpty
                  ? new AssetImage("assets/images/default.jpg")
                  : new NetworkImage(predica.txRutaImagen),
          radius: Device.isPhone() ? _screenSize.width * 0.12 : _screenSize.width * 0.07,
        ));
    final planetCard = new Container(
      width: _screenSize.width * 0.8,
      margin: new EdgeInsets.only(left: _screenSize.width * 0.13),
      decoration: new BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: new BorderRadius.circular(8.0),
        boxShadow: <BoxShadow>[
          new BoxShadow(
            color: Colors.white70,
            blurRadius: 10.0,
            offset: new Offset(0.0, 7.0),
          ),
        ],  
      ),
      child: Container(
          padding: EdgeInsets.fromLTRB(55.0, 10.0, 12.0, 10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(predica.txNombres,
                  style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
              Text(
                predica.txContenidoCorto,
                style: TextStyle(fontSize: 14.0),
                textAlign: TextAlign.justify,
              ),
              Row(
                children: <Widget>[
                  Icon(
                    Icons.favorite_border,
                    color: Colors.red,
                    size: 12.0,
                  ),
                  Text(' ${predica.qnLove} Me encanta',
                      style: TextStyle(fontSize: 12.0)),
                  SizedBox(
                    width: 10.0,
                  ),
                  Text('|', style: TextStyle(fontSize: 12.0)),
                  SizedBox(
                    width: 10.0,
                  ),
                  Icon(
                    Icons.sentiment_very_satisfied,
                    color: Colors.red,
                    size: 12.0,
                  ),
                  Text(' ${predica.qnLikes} Me gusta',
                      style: TextStyle(fontSize: 12.0)),
                  SizedBox(
                    width: 10.0,
                  ),
                ],
              ),
            ],
          )),
    );

    return new GestureDetector(
      child: new Container(
          height: _screenSize.height * 0.25,
          margin: const EdgeInsets.symmetric(
            vertical: 10.0,
            horizontal: 24.0,
          ),
          child: new Stack(
            children: <Widget>[
              planetCard,
              planetThumbnail,
            ],
          )),
      onTap: () => {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PredicasPage(predica: predica)),
        )
      },
    );
  }
}

class LikeMacPredica {
    int idPredica;
    String macaddres;
    bool bdLike;
    bool bdlove;

    LikeMacPredica({
        this.idPredica,
        this.macaddres,
        this.bdLike,
        this.bdlove,
    });

    factory LikeMacPredica.fromJson(Map<String, dynamic> json) => LikeMacPredica(
        idPredica: json["idPredica"] == null ? null : json["idPredica"],
        macaddres: json["macaddres"] == null ? null : json["macaddres"],
        bdLike: json["bdLike"] == null ? null : json["bdLike"],
        bdlove: json["bdlove"] == null ? null : json["bdlove"],
    );

    Map<String, dynamic> toJson() => {
        "idPredica": idPredica == null ? null : idPredica,
        "macaddres": macaddres == null ? null : macaddres,
        "bdLike": bdLike == null ? null : bdLike,
        "bdlove": bdlove == null ? null : bdlove,
    };
}
class Alertas {
  String imagen;
  String text;
  String contenido;
  Alertas();

  Alertas.fromJsonMap(Map<String,dynamic> json){
    text = json['txMensaje'];
    imagen = json['imagen'];
    contenido = json['contenido'];
  }

  List<Alertas> getAlertas(List<dynamic> jsonList){
    List<Alertas> listaAlertas = new List();
    if (jsonList == null) return listaAlertas;
    for (var item in jsonList){
      final alertas = new Alertas.fromJsonMap(item);
      listaAlertas.add(alertas);
    }
    return listaAlertas;
  }
}
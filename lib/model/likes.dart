class Likes{
  bool _like;
  int _countLike;

  
  bool _heart;
  int _countHeart;

  
  int _ciPredica;
  
	bool get like => _like;

  set like(bool like) {
    _like = like;
  }

  bool get heart => _heart;

  set heart(bool heart) {
    _heart = heart;
  }

  int get ciPredica => _ciPredica;

  set ciPredica(int ciPredica) {
    _ciPredica = ciPredica;
  }
  
  int get countLike => _countLike;

  set countLike(int countLike) {
    _countLike = countLike;
  }

  int get countHeart => _countHeart;

  set countHeart(int countHeart) {
    _countHeart = countHeart;
  }
}
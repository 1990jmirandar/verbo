class Predicas{
  String txTipoPredicas;
  int qnLove;
  int qnLikes;
  int idPredicas;
  String txNombres;
  String txContenido;
  String txRutaVideo;
  String txRutaImagen;
  int ciTipoPredica;
  String txContenidoCorto;
  String device;

  Predicas();
  Predicas.fromJsonMap(Map<String,dynamic> json){
    txTipoPredicas = json['txTipoPredicas'];
    qnLove = json['qnLove'];
    qnLikes = json['qnLikes'];
    idPredicas = json['idPredicas'];
    txNombres = json['txNombres'];
    txContenido = json['txContenido'];
    txRutaVideo = json['txRutaVideo'];
    txRutaImagen = json['txRutaImagen'];
    ciTipoPredica = json['ciTipoPredica'];
    txContenidoCorto = json['txContenidoCorto'];
  }


  List<Predicas> getPredicas(List<dynamic> jsonList){
    List<Predicas> listaPredicas = new List();
    if (jsonList == null) return listaPredicas;
    for (var item in jsonList){
      final predicas = new Predicas.fromJsonMap(item);
      listaPredicas.add(predicas);
    }
    return listaPredicas;
  }
}
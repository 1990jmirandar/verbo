class Tipo{
  String txNombres;
  int ciTipoPredica;

  Tipo();
  Tipo.fromJsonMap(Map<String,dynamic> json){
    txNombres = json['txNombres'];
    ciTipoPredica = json['ciTipoPredica'];
  }


  List<Tipo> getTipos(List<dynamic> jsonList){
    List<Tipo> listaTipos= new List();
    if (jsonList == null) return listaTipos;
    for (var item in jsonList){
      final tipo = new Tipo.fromJsonMap(item);
      listaTipos.add(tipo);
    }
    return listaTipos;
  }
}
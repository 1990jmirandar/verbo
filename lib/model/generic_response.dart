class Response {
  String codigoError;
  String mensajeError;

  Response();

  Response.fromJson(Map<String, dynamic> json) {
    codigoError = json["CodigoError"];
    mensajeError = json["MensajeError"];
  }

  Response response(dynamic json) {
    Response response = new Response();
    if (json == null) return response;
    response = new Response.fromJson(json);
    return response;
  }
}

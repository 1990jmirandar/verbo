import 'dart:async';

import 'package:flutter/material.dart';

class OracionBloc{
  final _nombreController = StreamController<String>.broadcast();
  final _telefonoController = StreamController<String>.broadcast();
  final _emailController = StreamController<String>.broadcast();
  final _oracionController = StreamController<String>.broadcast();

  Function(String) get changeEmail => _emailController.sink.add;
  Function(String) get changeNombre => _emailController.sink.add;
  Function(String) get changetelefono => _emailController.sink.add;
  Function(String) get changeOracion => _emailController.sink.add;

  Stream<String> get emailStream => _emailController.stream;
  Stream<String> get nombreStream => _nombreController.stream;
  Stream<String> get telefonoStream => _telefonoController.stream;
  Stream<String> get oracionStream => _oracionController.stream;
  

  dispose(){
    _nombreController.close();
    _telefonoController.close();
    _emailController.close();
    _oracionController.close();
  }


}
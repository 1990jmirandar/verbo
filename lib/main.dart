import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:verbo/pages/main_page.dart';
import 'package:verbo/utils/class_builder.dart';
import 'package:verbo/utils/device.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final device = Device();
  await device.getInfoDevice();
  ClassBuilder.registerClasses();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new MaterialApp(
      home: new MyApp(),
      color: Color.fromRGBO(14, 20, 32, 1),
      debugShowCheckedModeBanner: false,
      theme: ThemeData.light().copyWith(platform: TargetPlatform.android),
    ));
  });
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    return new SplashScreen(
      seconds: 3,
      navigateAfterSeconds: MainPage(),
      image: new Image.asset('assets/images/logo.png'),
      backgroundColor: Colors.white,
      photoSize: _screenSize.height * 0.15,
      loaderColor: Colors.transparent,
    );
  }
}

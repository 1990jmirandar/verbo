import 'dart:convert';

import 'package:verbo/model/predicas.dart';
import 'package:http/http.dart' as http;
import 'package:verbo/utils/constants.dart';

class PredicasProvider{
  Future<List<Predicas>> getPredicas() async {
    final url = "${Constants.URL}Api/values";
    final response = await http.get(url);
    final decodeData = json.decode(response.body);

    final List<Predicas> listaPredicas = new Predicas().getPredicas(decodeData);
    return listaPredicas;
  }
}
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:verbo/model/generic_response.dart';
import 'package:verbo/utils/constants.dart';
import 'package:verbo/utils/phone.dart';
class OracionesProvider {
  var phone = Phone();
  Future<Response> setOraciones(
      String nombre, String email, String telefono, String observacion) async {
    var url = '${Constants.URL}Api/Oraciones';
    final identifier = phone.uiIdentifier;
    var response = await http.post(url, body: {
      'txNombres': '$nombre',
      'txTelefono': '$telefono',
      'txEmail': '$email',
      'txObservacion': '$observacion',
      'UsuarioIngreso': '$identifier'
    });
    final decodeData = json.decode(response.body);

    final Response oracion = new Response().response(decodeData);
    print(oracion.mensajeError);
    return oracion;
  }
}

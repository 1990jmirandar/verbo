import 'package:unique_identifier/unique_identifier.dart';

class UniqueIdentifierDevice{

  Future<String> initUniqueIdentifierState() async {
    String identifier;
    try {
      identifier = await UniqueIdentifier.serial;
    } on Exception {
      identifier = 'Failed to get Unique Identifier';
    }
    return identifier;
  }
}
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:verbo/model/tipo.dart';
import 'package:verbo/providers/unique_identifier.dart';
import 'package:verbo/utils/constants.dart';
import 'package:verbo/utils/phone.dart';

class TipoProvider{
  var phone = Phone();
  Future<List<Tipo>> getTipos() async {
    final url = "${Constants.URL}Api/tipo";
    final response = await http.get(url);
    final decodeData = json.decode(response.body);
    final List<Tipo> listaTipos = new Tipo().getTipos(decodeData);
    var identifier = await UniqueIdentifierDevice().initUniqueIdentifierState();
    phone.uiIdentifier = identifier;
    return listaTipos;
  }
}
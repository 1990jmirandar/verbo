import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:verbo/model/generic_response.dart';
import 'package:verbo/model/like_mac_predica.dart';
import 'package:verbo/model/likes.dart';
import 'package:verbo/utils/constants.dart';
import 'package:verbo/utils/phone.dart';

class LikeProvider {
  var phone = Phone();
  Future<Response> setLike(
      int ciPredica, bool bdLove, bool bdLikes) async {
    var url = '${Constants.URL}Api/values/$ciPredica'  ;
    final identifier = phone.uiIdentifier;
    var response = await http.put(url, body: {
      'ciPredica': '$ciPredica',
      'bdLove': '$bdLove',
      'bdLikes': '$bdLikes',
      'MacAddres': '$identifier'
    });
    final decodeData = json.decode(response.body);
    final Response like = new Response().response(decodeData);
    print(like.mensajeError);
    if (phone.likes!=null){
      bool exist = false;
      for (int i = 0; i< phone.likes.length;i++){
        if (phone.likes[i].ciPredica == ciPredica){
          phone.likes[i].heart = bdLove;
          phone.likes[i].like = bdLikes;
          exist= true;
        }
      }
      if (!exist){
        final like = Likes();
        like.ciPredica= ciPredica;
        like.heart= bdLove;
        like.like = bdLikes;
        phone.likes.add(like);
      }
    }else{
      final like = Likes();
      like.ciPredica= ciPredica;
      like.heart= bdLove;
      like.like = bdLikes;
      phone.likes = List<Likes>();
      phone.likes.add(like);
    }
    
    return like;
  }


  Future<LikeMacPredica> getLike(
      int ciPredica) async {
    final identifier = phone.uiIdentifier;
    var url = '${Constants.URL}Api/values?mac=$identifier&idPredica=$ciPredica'  ;
    var response = await http.get(url);
    final decodeData = json.decode(response.body);
    final LikeMacPredica like = new LikeMacPredica.fromJson(decodeData[0]);
    print(decodeData);
    return like;
  }


}

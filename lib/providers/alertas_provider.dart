import 'dart:convert';
import 'package:verbo/model/alertas.dart';
import 'package:http/http.dart' as http;
import 'package:verbo/utils/constants.dart';



class AlertasProvider {
  Future<List<Alertas>> getAlertas() async {
    final url = "${Constants.URL}Api/alertas";
    final response = await http.get(url);
    final decodeData = json.decode(response.body);
    final List<Alertas> listaAlertas = new Alertas().getAlertas(decodeData);
    return listaAlertas;
  }
}

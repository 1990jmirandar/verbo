import 'package:verbo/model/likes.dart';

class Phone {
  String _nombre;
  String _correo;
  List<Likes> _likes;
  String _uiIdentifier;

  

  static final Phone _singleton = Phone._internal();

  factory Phone() {
    return _singleton;
  }

  Phone._internal();

  String get nombre => _nombre;

  set nombre(String nombre) {
    _nombre = nombre;
  }

  String get correo => _correo;

  set correo(String correo) {
    _correo = correo;
  }

  List<Likes> get likes => _likes;

  set likes(List<Likes> likes) {
    _likes = likes;
  }

  String get uiIdentifier => _uiIdentifier;

  set uiIdentifier(String uiIdentifier) {
    _uiIdentifier = uiIdentifier;
  }
}



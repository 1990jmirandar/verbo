import 'package:verbo/pages/content_page.dart';
import 'package:verbo/pages/lista_anuncios_page.dart';
import 'package:verbo/pages/lista_devocionales_page.dart';
import 'package:verbo/pages/lista_informativos_page.dart';
import 'package:verbo/pages/lista_musica_page.dart';
import 'package:verbo/pages/lista_predica.dart';

typedef T Constructor<T>();

final Map<String, Constructor<Object>> _constructors = <String, Constructor<Object>>{};

void register<T>(Constructor<T> constructor) {
  _constructors[T.toString()] = constructor;
}

class ClassBuilder {
  static void registerClasses() {
    register<ContentPage>(() => ContentPage());
    register<ListaPredicaPage>(() => ListaPredicaPage());
    register<InformativosPage>(() => InformativosPage());
    register<MusicaPage>(() => MusicaPage());
    register<DevocionalesPage>(() => DevocionalesPage());
    register<AnunciosPage>(() => AnunciosPage());
  }

  static String page= "ContentPage";

  static dynamic fromString() {
    return _constructors[page]();
  }
}
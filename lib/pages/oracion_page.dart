import 'package:flutter/material.dart';
import 'package:verbo/providers/oraciones_provider.dart';

void main() => runApp(OracionPage());

class OracionPage extends StatefulWidget {
  @override
  OracionPageState createState() {
    return OracionPageState();
  }
}

class OracionPageState extends State<OracionPage> {
  final _formKey = GlobalKey<FormState>();
  static const double paddingTextField = 16.0;
  final nombreController = TextEditingController();
  final telefonoController = TextEditingController();
  final emailController = TextEditingController();
  final observacionController = TextEditingController();

  @override
  void dispose() {
    nombreController.dispose();
    telefonoController.dispose();
    emailController.dispose();
    observacionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    return Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: _screenSize.width * 0.05),
              Padding(
                padding: const EdgeInsets.fromLTRB(paddingTextField, 0, 0, 0),
                child: Text('Oraciones',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 50.0)),
              ),
              Padding(
                  padding:
                      const EdgeInsets.fromLTRB(paddingTextField, 0, 40, 0),
                  child: Text('¡Ingresa tus datos y envianos tus oraciones!',
                      style: TextStyle(fontSize: 30.0, color: Colors.black45))),
              Image.asset(
                "assets/images/oracion.png",
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(paddingTextField,
                    paddingTextField * 2, paddingTextField, 0),
                child: TextFormField(
                  controller: nombreController,
                  maxLength: 50,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Por favor ingrese el nombre';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      labelText: 'Nombre',
                      hintText: 'Ejemplo: Javier',
                      icon: Icon(
                        Icons.person,
                        color: Colors.blue,
                      )),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(
                    paddingTextField, 0, paddingTextField, 0),
                child: TextFormField(
                  controller: telefonoController,
                  maxLength: 10,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Por favor ingrese el número de teléfono';
                    }
                    return null;
                  },
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(
                      labelText: 'Teléfono',
                      hintText: 'Ejemplo: 04-123-123',
                      icon: Icon(
                        Icons.phone_android,
                        color: Colors.blue,
                      )),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(
                    paddingTextField, 0, paddingTextField, 0),
                child: TextFormField(
                  controller: emailController,
                  maxLength: 50,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Por favor ingrese el email';
                    }
                    return null;
                  },
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      labelText: 'Email',
                      hintText: 'Ejemplo: jose_vicente89@gmail.com',
                      icon: Icon(
                        Icons.contact_mail,
                        color: Colors.blue,
                      )),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(
                    paddingTextField, 0, paddingTextField, 0),
                child: TextFormField(
                  controller: observacionController,
                  maxLength: 200,
                  maxLines: 3,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Por favor ingrese la oración';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      labelText: 'Oración',
                      icon: Icon(
                        Icons.accessibility_new,
                        color: Colors.blue,
                      )),
                ),
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(
                          paddingTextField, 0, paddingTextField, 0),
                      child: RaisedButton(
                        textColor: Colors.white,
                        color: Colors.blue,
                        onPressed: () async {
                          FocusScopeNode currentFocus = FocusScope.of(context);
                          if (!currentFocus.hasPrimaryFocus) {
                            currentFocus.unfocus();
                          }
                          if (_formKey.currentState.validate()) {
                            Scaffold.of(context).showSnackBar(
                                SnackBar(content: Text('Oración Enviada')));
                            OracionesProvider oracionProvider =
                                new OracionesProvider();
                            final result = await oracionProvider.setOraciones(
                                nombreController.text,
                                emailController.text,
                                telefonoController.text,
                                observacionController.text);
                            if (result.codigoError == "000") {
                              Scaffold.of(context).showSnackBar(
                                  SnackBar(content: Text('Oración Recibida')));
                              nombreController.text = "";
                              emailController.text = "";
                              telefonoController.text = "";
                              observacionController.text = "";
                            } else {
                              Scaffold.of(context).showSnackBar(SnackBar(
                                  content: Text(
                                      'No pudo enviar la oración con éxito')));
                            }
                          }
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Text('Enviar oración'),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ));
  }

  /*Widget _insertarOracion(
      String nombre, String email, String telefono, String observacion) {
    OracionesProvider oracionProvider = new OracionesProvider();
    return FutureBuilder<Oraciones>(
      future:
          oracionProvider.setOraciones(nombre, email, telefono, observacion),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          Scaffold.of(context)
              .showSnackBar(SnackBar(content: Text('Oración recibida')));
        } else {
          return Center(child: new CircularProgressIndicator());
        }
      },
    );
  }*/
}

import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:rich_alert/rich_alert.dart';
import 'package:verbo/pages/contact_page.dart';
import 'package:verbo/providers/alertas_provider.dart';
import 'package:verbo/widgets/alert_swiper.dart';

import 'oracion_page.dart';

class ContentPage extends KFDrawerContent {
  ContentPage({
    Key key,
  });
  @override
  ContentPageState createState() => ContentPageState();
}

class ContentPageState extends State<ContentPage> {
  //Refactorizar
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  _register() {
    _firebaseMessaging
        .getToken()
        .then((token) => token);
    _firebaseMessaging.subscribeToTopic("news");
  }

  @override
  void initState() {
    super.initState();
    _register();
    getMessage();
  }

  void getMessage() {
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print('on message $message');
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return RichAlertDialog(
                  //uses the custom alert dialog
                  alertTitle: richTitle(message['aps']['alert']['title']),
                  alertSubtitle: richSubtitle(message['aps']['alert']['body']),
                  alertType: RichAlertType.SUCCESS,
                );
              });
        },
        onResume: (Map<String, dynamic> message) async {
          print('on resume $message');
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return RichAlertDialog(
                  //uses the custom alert dialog
                  alertTitle: richTitle(message['aps']['alert']['title']),
                  alertSubtitle: richSubtitle(message['aps']['alert']['body']),
                  alertType: RichAlertType.SUCCESS,
                );
              });
        },
        onLaunch: (Map<String, dynamic> message) async {
          print('on launch $message');
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return RichAlertDialog(
                  alertTitle: richTitle(message['aps']['alert']['title']),
                  alertSubtitle: richSubtitle(message['aps']['alert']['body']),
                  alertType: RichAlertType.SUCCESS,
                );
              });
        });
  }

  var currentPage = 0;

  final AlertasProvider alertasProvider = new AlertasProvider();
  GlobalKey bottomNavigationKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      bottomNavigationBar: FancyBottomNavigation(
        initialSelection: 0,
        key: bottomNavigationKey,
        tabs: [
          TabData(iconData: Icons.home, title: "Inicio"),
          TabData(
            iconData: Icons.contacts,
            title: "Contactos",
          ),
          TabData(
            iconData: Icons.accessibility_new,
            title: "Oraciones",
          ),
        ],
        onTabChangedListener: (position) {
          setState(() {
            currentPage = position;
          });
        },
      ),
      body: _getPage(currentPage),
      appBar: AppBar(
        titleSpacing: 0.0,
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(32.0)),
              child: Material(
                shadowColor: Colors.transparent,
                color: Colors.transparent,
                child: IconButton(
                    icon: Icon(
                      Icons.menu,
                      color: Colors.black,
                    ),
                    onPressed: widget.onMenuPressed),
              ),
            ),
          ],
        ),
        automaticallyImplyLeading: false,
        centerTitle: true,
      ),
    );
  }

  _getPage(int page) {
    final _screenSize = MediaQuery.of(context).size;
    switch (page) {
      case 0:
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: _screenSize.width * 0.05),
            Padding(
              padding: const EdgeInsets.fromLTRB(16, 0, 0, 0),
              child: Text('Verbo',
                  style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 50.0)),
            ),
            Padding(
                padding: const EdgeInsets.fromLTRB(16, 0, 40, 0),
                child: Text('Una familia discipulando a las naciones!',
                    style: TextStyle(fontSize: 30.0, color: Colors.black45))),
            SizedBox(height: _screenSize.width * 0.10),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 0.0),
                child: _swiperAlertas(),
              ),
            ),
          ],
        );
      case 1:
        return Container(child: new ContanctPage());
      case 2:
        return Container(child: new OracionPage());
      default:
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text("This is the basket page"),
            RaisedButton(
              child: Text(
                "Start new page",
                style: TextStyle(color: Colors.white),
              ),
              color: Theme.of(context).primaryColor,
              onPressed: () {},
            )
          ],
        );
    }
  }

  Widget _swiperAlertas() {
    return FutureBuilder(
      future: alertasProvider.getAlertas(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return AlertaSwiper(listAlert: snapshot.data);
        } else {
          return Center(child: new CircularProgressIndicator());
        }
      },
    );
  }
}

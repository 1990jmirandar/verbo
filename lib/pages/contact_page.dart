import 'package:flutter/material.dart';

void main() => runApp(ContanctPage());

class ContanctPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        SizedBox(height: _screenSize.width * 0.05),
        Image.asset("assets/images/contact.png"),
        SizedBox(height: 7.0),
        Container(
          margin: const EdgeInsets.symmetric(
            vertical: 7.0,
            horizontal: 24.0,
          ),
          height: 40.0,
          padding: EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              new Image(
                image: new AssetImage("assets/images/icon_phone.png"),
              ),
              SizedBox(width: 7.0),
              Text(
                '02-244-1706',
              )
            ],
          ),
          decoration: getDecorationCard(),
        ),
        Container(
          margin: const EdgeInsets.symmetric(
            vertical: 7.0,
            horizontal: 24.0,
          ),
          height: 40.0,
          padding: EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              new Image(
                image: new AssetImage("assets/images/icon_email.png"),
              ),
              SizedBox(width: 7.0),
              Text('Info@verboecuador.org')
            ],
          ),
          decoration: getDecorationCard(),
        ),
        Container(
          margin: const EdgeInsets.symmetric(
            vertical: 7.0,
            horizontal: 24.0,
          ),
          height: 40.0,
          padding: EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              new Image(
                image: new AssetImage("assets/images/icon_internet.png"),
              ),
              SizedBox(width: 7.0),
              Text('http://verboecuador.org/')
            ],
          ),
          decoration: getDecorationCard(),
        ),
        Container(
          margin: const EdgeInsets.symmetric(
            vertical: 7.0,
            horizontal: 24.0,
          ),
          height: 40.0,
          padding: EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              new Image(
                image: new AssetImage("assets/images/icon_facebook.png"),
              ),
              SizedBox(width: 7.0),
              Text('https://www.facebook.com/iglesia.verbo')
            ],
          ),
          decoration: getDecorationCard(),
        ),
      ],
    );
  }

  BoxDecoration getDecorationCard() {
    return new BoxDecoration(
      color: new Color(0xFF448AFF),
      shape: BoxShape.rectangle,
      borderRadius: new BorderRadius.circular(8.0),
      boxShadow: <BoxShadow>[
        new BoxShadow(
          color: Colors.black12,
          blurRadius: 10.0,
          offset: new Offset(0.0, 10.0),
        ),
      ],
      gradient: LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [Colors.white, Colors.blue],
        tileMode: TileMode.repeated,
      ),
    );
  }
}

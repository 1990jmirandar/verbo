import 'dart:async';

import 'package:auto_orientation/auto_orientation.dart';
import 'package:chewie/chewie.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rich_alert/rich_alert.dart';
import 'package:video_player/video_player.dart';

class ChewieDemo extends StatefulWidget {
  ChewieDemo({this.title = 'Chewie Demo', this.video});

  final String title;
  final String video;

  @override
  State<StatefulWidget> createState() {
    return _ChewieDemoState(video: this.video);
  }
}

class _ChewieDemoState extends State<ChewieDemo> {
  VideoPlayerController _videoPlayerController1;
  ChewieController _chewieController;
  String video;
  _ChewieDemoState({this.video});
  _ChewieDemoState.forDesignTime();

//Refactorizar
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  _register() {
    _firebaseMessaging.getToken().then((token) => print(token));
    _firebaseMessaging.subscribeToTopic("news");
  }

  void getMessage() {
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
      print('on message $message');
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return RichAlertDialog(
              //uses the custom alert dialog
              alertTitle: richTitle(message['aps']['alert']['title']),
              alertSubtitle: richSubtitle(message['aps']['alert']['body']),
              alertType: RichAlertType.SUCCESS,
            );
          });
    }, onResume: (Map<String, dynamic> message) async {
      print('on resume $message');
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return RichAlertDialog(
              //uses the custom alert dialog
              alertTitle: richTitle(message['aps']['alert']['title']),
              alertSubtitle: richSubtitle(message['aps']['alert']['body']),
              alertType: RichAlertType.SUCCESS,
            );
          });
    }, onLaunch: (Map<String, dynamic> message) async {
      print('on launch $message');
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return RichAlertDialog(
              alertTitle: richTitle(message['aps']['alert']['title']),
              alertSubtitle: richSubtitle(message['aps']['alert']['body']),
              alertType: RichAlertType.SUCCESS,
            );
          });
    });
  }

  @override
  void initState() {
    super.initState();
    _videoPlayerController1 = VideoPlayerController.network(video);
    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController1,
      aspectRatio: 3 / 2,
      autoPlay: true,
      looping: true,
      routePageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondAnimation, provider) {
        return AnimatedBuilder(
          animation: animation,
          builder: (BuildContext context, Widget child) {
            return VideoScaffold(
              child: Scaffold(
                resizeToAvoidBottomPadding: false,
                body: Container(
                  alignment: Alignment.center,
                  color: Colors.black,
                  child: provider,
                ),
              ),
            );
          },
        );
      },
      // Try playing around with some of these other options:

      showControls: true,
      materialProgressColors: ChewieProgressColors(
        playedColor: Colors.red,
        handleColor: Colors.blue,
        backgroundColor: Colors.black,
        bufferedColor: Colors.grey,
      ),
      placeholder: Container(
        color: Colors.grey,
      ),
      autoInitialize: false,
    );
    _register();
    getMessage();
  }

  @override
  void dispose() {
    _videoPlayerController1.dispose();
    _chewieController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        titleSpacing: 0.0,
        backgroundColor: Colors.white,
        elevation: 0.0,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(height: _screenSize.width * 0.05),
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 0, 0),
            child: Text('Video',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 50.0)),
          ),
          Padding(
              padding: const EdgeInsets.fromLTRB(16, 0, 40, 0),
              child: Text('Disfruta de nuestros videos!',
                  style: TextStyle(fontSize: 30.0, color: Colors.black45))),
          Expanded(
            child: Chewie(
              controller: _chewieController,
            ),
          ),
        ],
      ),
    );
  }
}

class VideoScaffold extends StatefulWidget {
  const VideoScaffold({Key key, this.child}) : super(key: key);

  final Widget child;

  @override
  State<StatefulWidget> createState() => _VideoScaffoldState();
}

class _VideoScaffoldState extends State<VideoScaffold> {
  @override
  void initState() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
    AutoOrientation.landscapeAutoMode();
    super.initState();
  }

  @override
  dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    AutoOrientation.portraitAutoMode();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}

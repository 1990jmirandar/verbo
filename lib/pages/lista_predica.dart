import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:rich_alert/rich_alert.dart';
import 'package:verbo/providers/predicas_provider.dart';
import 'package:verbo/widgets/predicas_list_view.dart';

class ListaPredicaPage extends KFDrawerContent {
  int idTipo = 1;
  ListaPredicaPage(){

  }
  @override
  ListContentPageState createState() => ListContentPageState(idTipo);
}

class ListContentPageState extends State<ListaPredicaPage> {
  int idTipo;

  ListContentPageState(int idTipo) {
    this.idTipo = idTipo;
  }

  //Refactorizar
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  
    _register() {
      _firebaseMessaging.getToken().then((token) => print(token));
      _firebaseMessaging.subscribeToTopic("news");
    }
  
    @override
    void initState() {
      super.initState();
      _register();
      getMessage();
    }
  
    void getMessage() {
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print('on message $message');
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return RichAlertDialog(
                  //uses the custom alert dialog
                  alertTitle: richTitle(message['aps']['alert']['title']),
                  alertSubtitle: richSubtitle(message['aps']['alert']['body']),
                  alertType: RichAlertType.SUCCESS,
                );
              });
        },
        onResume: (Map<String, dynamic> message) async {
          print('on resume $message');
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return RichAlertDialog(
                  //uses the custom alert dialog
                  alertTitle: richTitle(message['aps']['alert']['title']),
                  alertSubtitle: richSubtitle(message['aps']['alert']['body']),
                  alertType: RichAlertType.SUCCESS,
                );
              });
        },
        onLaunch: (Map<String, dynamic> message) async {
          print('on launch $message');
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return RichAlertDialog(
                  alertTitle: richTitle(message['aps']['alert']['title']),
                  alertSubtitle: richSubtitle(message['aps']['alert']['body']),
                  alertType: RichAlertType.SUCCESS,
                );
              });
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromRGBO(8, 28, 37, 1),
        appBar: AppBar(
          titleSpacing: 0.0,
          backgroundColor: Color.fromRGBO(8, 28, 37, 1),
          elevation: 0.0,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(32.0)),
                child: Material(
                  shadowColor: Colors.transparent,
                  color: Colors.transparent,
                  child: IconButton(
                    icon: Icon(
                      Icons.menu,
                      color: Colors.white,
                    ),
                    onPressed: widget.onMenuPressed,
                  ),
                ),
              ),
            ],
          ),
          automaticallyImplyLeading: false,
          centerTitle: true,
        ),
        body: _listViewPredicas());
  }

  Widget _listViewPredicas() {
    return FutureBuilder(
      future: PredicasProvider().getPredicas(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return PredicasListView(listPredicas: snapshot.data, idTipo: idTipo, currentPage: "ListaPredicaPage");
        } else {
          return Center(child: new CircularProgressIndicator());
        }
      },
    );
  }
}

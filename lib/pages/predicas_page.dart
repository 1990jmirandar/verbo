import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:like_button/like_button.dart';
import 'package:rich_alert/rich_alert.dart';
import 'package:verbo/model/like_mac_predica.dart';
import 'package:verbo/model/likes.dart';
import 'package:verbo/model/predicas.dart';
import 'package:verbo/pages/video_player_page.dart';
import 'package:verbo/providers/like_provider.dart';
import 'package:verbo/utils/device.dart';
import 'package:verbo/utils/phone.dart';

class PredicasPage extends KFDrawerContent {
  Predicas predica;

  PredicasPage({
    Predicas predica,
    Key key,
  }) {
    this.predica = predica;
  }

  @override
  DetailPageState createState() => DetailPageState(predica: predica);
}

class DetailPageState extends State<PredicasPage> {
  Predicas predica;
  Likes like;
  double buttonSize = 30.0;

  final likeProvider = LikeProvider();
  final phone = Phone();

  DetailPageState({Predicas predica}) {
    this.predica = predica;
    this.like = Likes();
    like.countLike = predica.qnLikes;
    like.countHeart = predica.qnLove;
    bool exist = false;
    if (phone.likes != null) {
      for (int i = 0; i < phone.likes.length; i++) {
        if (phone.likes[i].ciPredica == predica.idPredicas) {
          like.heart = phone.likes[i].heart;
          like.like = phone.likes[i].like;
          exist = true;
        }
      }

      if (!exist) {
        like.heart = false;
        like.like = false;
      }
    } else {
      like.heart = false;
      like.like = false;
    }
  }

  //Refactorizar
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  _register() {
    _firebaseMessaging.getToken().then((token) => print(token));
    _firebaseMessaging.subscribeToTopic("news");
  }

  @override
  void initState() {
    super.initState();
    _register();
    getMessage();
  }

  void getMessage() {
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print('on message $message');
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return RichAlertDialog(
                  //uses the custom alert dialog
                  alertTitle: richTitle(message['aps']['alert']['title']),
                  alertSubtitle: richSubtitle(message['aps']['alert']['body']),
                  alertType: RichAlertType.SUCCESS,
                );
              });
        },
        onResume: (Map<String, dynamic> message) async {
          print('on resume $message');
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return RichAlertDialog(
                  //uses the custom alert dialog
                  alertTitle: richTitle(message['aps']['alert']['title']),
                  alertSubtitle: richSubtitle(message['aps']['alert']['body']),
                  alertType: RichAlertType.SUCCESS,
                );
              });
        },
        onLaunch: (Map<String, dynamic> message) async {
          print('on launch $message');
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return RichAlertDialog(
                  alertTitle: richTitle(message['aps']['alert']['title']),
                  alertSubtitle: richSubtitle(message['aps']['alert']['body']),
                  alertType: RichAlertType.SUCCESS,
                );
              });
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(8, 28, 37, 1),
          elevation: 0.0,
          iconTheme: IconThemeData(
            color: Colors.white, //change your color here
          ),
        ),
        backgroundColor: Color.fromRGBO(8, 28, 37, 1),
        body: SingleChildScrollView(
          child: Column(children: <Widget>[
            crearImagen(),
            crearTitulo(),
            SizedBox(height: 16.0),
            crearContenido(),
            SizedBox(
              height: 16.0,
            )
          ]),
        ));
  }

  Widget crearImagen() {
    final _screenSize = MediaQuery.of(context).size;
    return new Container(
        margin: EdgeInsets.fromLTRB(
            0, _screenSize.height * 0.03, 0, _screenSize.height * 0.03),
        child: CircleAvatar(
          backgroundColor: Color.fromRGBO(8, 28, 37, 1),
          backgroundImage: predica.txRutaImagen.isEmpty
              ? new AssetImage("assets/images/default.jpg")
              : new NetworkImage(predica.txRutaImagen),
          radius: Device.isPhone() ? 90 :_screenSize.height * 0.2,
        ));
  }

  Widget crearTitulo() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 0),
      padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 20.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Color.fromRGBO(255, 255, 255, 0.4)),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(predica.txNombres,
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white)),
                SizedBox(
                  height: 05.0,
                ),
                likeCount(context)
              ],
            ),
          ),
          predica.txRutaVideo != null && predica.txRutaVideo.isNotEmpty
              ? InkWell(
                  onTap: () => {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              //VideoPlayerScreen(url: predica.txRutaVideo)
                              ChewieDemo(video: predica.txRutaVideo)),
                    )
                  },
                  child: Icon(
                    Icons.play_circle_filled,
                    color: Color.fromRGBO(225, 0, 0, 1),
                    size: 50.0,
                  ),
                )
              : Text(""),
        ],
      ),
    );
  }

  Widget likeCount(BuildContext context) {
    return FutureBuilder<LikeMacPredica>(
      future: LikeProvider().getLike(predica.idPredicas),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Row(
            children: <Widget>[
              LikeButton(
                size: buttonSize,
                likeCount: like.countHeart,
                isLiked: snapshot.data.bdlove,
                countBuilder: (int count, bool isLiked, String text) {
                  var color = isLiked ? Colors.white : Colors.grey;
                  Widget result;
                  if (count > 0) {
                    result = Text(
                      text,
                      style: TextStyle(color: color),
                    );
                  }
                  return result;
                },
                onTap: (bool isLiked) {
                  return onLikeButtonTap(isLiked, like, "heart");
                },
                likeCountAnimationType: like.countLike < 1000
                    ? LikeCountAnimationType.part
                    : LikeCountAnimationType.none,
                likeCountPadding: EdgeInsets.only(left: 5.0),
              ),
              SizedBox(
                width: 20,
              ),
              LikeButton(
                size: buttonSize,
                isLiked: snapshot.data.bdLike,
                circleColor: CircleColor(
                    start: Color(0xff00ddff), end: Color(0xff0099cc)),
                bubblesColor: BubblesColor(
                  dotPrimaryColor: Color(0xff33b5e5),
                  dotSecondaryColor: Color(0xff0099cc),
                ),
                likeBuilder: (bool isLiked) {
                  return Icon(
                    Icons.sentiment_very_satisfied,
                    color: isLiked ? Colors.blueAccent : Colors.grey,
                    size: buttonSize,
                  );
                },
                likeCount: like.countLike,
                countBuilder: (int count, bool isLiked, String text) {
                  var color = isLiked ? Colors.white : Colors.grey;

                  Widget result;
                  if (count > 0) {
                    result = Text(
                      text,
                      style: TextStyle(color: color),
                    );
                  }

                  return result;
                },
                onTap: (bool isLiked) {
                  return onLikeButtonTap(isLiked, like, "like");
                },
                likeCountPadding: EdgeInsets.only(left: 5.0),
              ),
            ],
          );
        } else {
          return CircularProgressIndicator();
        }
      },
    );
  }

  Future<bool> onLikeButtonTap(bool isLiked, Likes like, String type) {
    final Completer<bool> completer = new Completer<bool>();
    if (type == "like") {
      Timer(const Duration(milliseconds: 200), () {
        like.like = !like.like;
        like.countLike = like.like ? like.countLike + 1 : like.countLike - 1;
        likeProvider.setLike(predica.idPredicas, like.heart, like.like);
        completer.complete(like.like);
      });
    } else {
      Timer(const Duration(milliseconds: 200), () {
        like.heart = !like.heart;
        like.countHeart =
            like.heart ? like.countHeart + 1 : like.countHeart - 1;

        likeProvider.setLike(predica.idPredicas, like.heart, like.like);
        completer.complete(like.heart);
      });
    }

    return completer.future;
  }

  Widget crearContenido() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 0),
      padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 20.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0), color: Colors.white),
      child: Html(
        data: """
              ${predica.txContenido}
              """,
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:verbo/providers/tipo_provider.dart';
import 'package:verbo/utils/phone.dart';
import 'package:verbo/widgets/menu.dart';
 
 

class MainPage extends StatefulWidget {
  MainPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  MainPageState createState() => MainPageState();
}

class MainPageState extends State<MainPage>  {
  var phone = Phone();

  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _listaMenu(),
    );
  }


  Widget _listaMenu(){
    return FutureBuilder(
      future: TipoProvider().getTipos(),
      builder: (BuildContext context, AsyncSnapshot snapshot){
        if (snapshot.hasData){
          return Menu(listTipo: snapshot.data);
        }else{
          return Center(child: new CircularProgressIndicator());
        }
      },
    );
  }
}